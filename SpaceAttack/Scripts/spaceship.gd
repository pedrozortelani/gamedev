extends KinematicBody2D

export (int) var speed = 7
var velocity = Vector2()
var alive = true
var health = 5

var bullet = load('res://Scenes/Bullet.tscn')

func get_input():
	
	velocity = Vector2.ZERO
	
	if alive:
		
		if Input.is_action_pressed('right'):
			velocity.x += 1 
		if Input.is_action_pressed('left'):
			velocity.x -= 1 
		if Input.is_action_pressed('down'):
			velocity.y += 1 
		if Input.is_action_pressed('up'):
			velocity.y -= 1 
		if Input.is_action_just_pressed('click'):
			shoot()
		velocity = velocity.normalized() * speed
		
func _physics_process(delta):
	get_input()
	var collision = move_and_collide(velocity)
	position.x = clamp(position.x, 24, ProjectSettings.get_setting('display/window/size/width') - 24)
	position.y = clamp(position.y, 24, ProjectSettings.get_setting('display/window/size/height') - 24)

func shoot():
	var new_bullet = bullet.instance()
	get_parent().add_child(new_bullet)
	new_bullet.set_position(self.position)
	new_bullet.velocity = get_global_mouse_position() - self.position 
