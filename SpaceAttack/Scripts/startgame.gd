extends Button

func _ready():
	self.connect('pressed', self, '_on_button_pressed')

func _on_button_pressed():
	get_tree().change_scene('res://Scenes/SpaceAttack.tscn')
