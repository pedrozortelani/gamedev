extends KinematicBody2D

export (int) var speed = 10
var velocity = Vector2()

func _ready():
	collision_layer = 2
	collision_mask = 2

func disappear():
	queue_free()
	
func _physics_process(delta):
	var collision = move_and_collide(velocity.normalized() * speed)
