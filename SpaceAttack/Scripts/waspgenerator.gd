extends KinematicBody2D

export (int) var speed = 600
var velocity = Vector2()
var moveX = 0
var moveY = 0
var wasp_queen = load('res://Scenes/AlienWasp.tscn')

var can_create = true
var timer = null
var spawn_delay = 2.0

func _ready():
	timer = Timer.new()
	timer.set_autostart(true)
	timer.set_wait_time(spawn_delay)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)

func on_timeout_complete():
	can_create = true

func get_velocity():
	
	if get_position().x < -24 && get_position().y < -24:
		moveX = 1
		moveY = 0
	elif get_position().x > ProjectSettings.get_setting('display/window/size/width') + 48 && get_position().y < -24:
		moveX = 0
		moveY = 1
	elif get_position().x > ProjectSettings.get_setting('display/window/size/width') + 48 && get_position().y > ProjectSettings.get_setting('display/window/size/height') + 48:
		moveX = -1
		moveY = 0
	elif get_position().x < -24 && get_position().y > ProjectSettings.get_setting('display/window/size/height') + 48:
		moveX = 0
		moveY = -1
		
	velocity.x = moveX
	velocity.y = moveY
	velocity = velocity.normalized() * speed

func generate_wasp():
	var new_wasp = wasp_queen.instance()
	get_parent().add_child(new_wasp)
	new_wasp.set_position(self.position)
	new_wasp.speed = 3
	can_create = false
	
	if spawn_delay > 0.3:
		spawn_delay -= 0.1
		timer.set_wait_time(spawn_delay)
	
func _physics_process(delta):
	get_velocity()
	velocity = move_and_slide(velocity)
	
	if can_create: 
		generate_wasp()
