extends KinematicBody2D

export (int) var speed = 0
var velocity = Vector2()
onready var target = get_node('../SpaceShip')
onready var hud_health = get_node('../HUD/Health/Label')
onready var hud_killcount = get_node('../HUD/KillCount/Label')

func _ready():
	collision_layer = 2
	collision_mask = 1

func get_velocity():
	
	velocity = target.global_position - get_position()
	velocity = velocity.normalized() * speed
		
func _physics_process(delta):
	get_velocity()
	var collision = move_and_collide(velocity)
	if collision && collision.collider.name == 'SpaceShip':
		cause_damage()
		die()
	if collision && 'Bullet' in collision.collider.name:
		collision.collider.disappear()
		die()
		
func cause_damage():
	target.health -= 1
	hud_health.set_text('X ' + str(target.health))
	if target.health == 0:
		get_tree().paused = true
		var gameover = get_node('../GameOver')
		gameover.show()

func die():
	queue_free()
	var killcount = int(hud_killcount.get_text().split(' ')[1])
	killcount += 1
	hud_killcount.set_text('X ' + str(killcount))
